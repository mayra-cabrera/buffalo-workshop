package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/mayra-cabrera/bloggy/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
