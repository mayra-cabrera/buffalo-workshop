# Buffalo Workshop

<div style="text-align:center" markdown="1">
  <img src="buffalo.png" height="300px" alt="buffalo logo"/>
</div>

## Content

1. Prework: Getting started with Go
2. Introduction to Buffalo
3. Creating our first application
4. Working with CRUD’s
5. Migrations
6. Forms
7. Validations
8. Deployment

Slides for the different sections can be found [here](buffalo-workshop-slides.pdf)

The content of this workshop is based on the original [Buffalo Workshop](https://www.gopherguides.com/workshops/buffalo-bootcamp-december-201) by [Mark Bates](https://twitter.com/markbates).
