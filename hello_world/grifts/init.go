package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/mayra-cabrera/hello_world/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
