package main

import (
	"log"

	"gitlab.com/mayra-cabrera/hello_world/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
